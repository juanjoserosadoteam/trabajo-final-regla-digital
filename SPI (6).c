#define AddrFIO0DIR 0x2009C000
#define AddrFIO0SET 0x2009C018
#define AddrFIO0CLR 0x2009C01C

unsigned int volatile *const FIO0DIR =(unsigned int *)AddrFIO0DIR;
unsigned int volatile *const FIO0SET =(unsigned int *)AddrFIO0SET;
unsigned int volatile *const FIO0CLR =(unsigned int*)AddrFIO0CLR;

#define AddrPCONP    0x400FC0C4
#define AddrPCLKSEL0 0x400FC1A8
#define AddrPINSEL0  0x4002C000
#define AddrPINSEL1  0x4002C004
#define AddrPINMODE0  0x4002C040
#define AddrPINMODE1  0x4002C044
#define AddrISER0	 0xE000E100


unsigned int volatile *const PCONP = (unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL0 = (unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PINSEL0 = (unsigned int*)AddrPINSEL0;
unsigned int volatile *const PINSEL1 = (unsigned int*)AddrPINSEL1;
unsigned int volatile *const PINMODE0 = (unsigned int*)AddrPINMODE0;
unsigned int volatile *const PINMODE1 = (unsigned int*)AddrPINMODE1;
unsigned int volatile *const ISER0 = (unsigned int*)AddrISER0;

//REGISTROS DE SPI

#define AddrS0SPCR  0x40020000
#define AddrS0SPSR  0x40020004
#define AddrS0SPDR  0x40020008
#define AddrS0SPCCR 0x4002000C
#define AddrS0SPINT 0x4002001C

unsigned int volatile *const S0SPCR =(unsigned int*)AddrS0SPCR;
unsigned int volatile *const S0SPSR =(unsigned int*)AddrS0SPSR;
unsigned int volatile *const S0SPDR =(unsigned int*)AddrS0SPDR;
unsigned int volatile *const S0SPCCR =(unsigned int*)AddrS0SPCCR;
unsigned int volatile *const S0SPINT =(unsigned int*)AddrS0SPINT;

//ADXL345 Register Addresses
#define	DEVID		0x00	//Device ID Register
#define THRESH_TAP	0x1D	//Tap Threshold
#define	OFSX		0x1E	//X-axis offset
#define	OFSY		0x1F	//Y-axis offset
#define	OFSZ		0x20	//Z-axis offset
#define	DURATION	0x21	//Tap Duration
#define	LATENT		0x22	//Tap latency
#define	WINDOW		0x23	//Tap window
#define	THRESH_ACT	0x24	//Activity Threshold
#define	THRESH_INACT	0x25	//Inactivity Threshold
#define	TIME_INACT	0x26	//Inactivity Time
#define	ACT_INACT_CTL	0x27	//Axis enable control for activity and inactivity detection
#define	THRESH_FF	0x28	//free-fall threshold
#define	TIME_FF		0x29	//Free-Fall Time
#define	TAP_AXES	0x2A	//Axis control for tap/double tap
#define ACT_TAP_STATUS	0x2B	//Source of tap/double tap
#define	BW_RATE		0x2C	//Data rate and power mode control
#define POWER_CTL	0x2D	//Power Control Register
#define	INT_ENABLE	0x2E	//Interrupt Enable Control
#define	INT_MAP		0x2F	//Interrupt Mapping Control
#define	INT_SOURCE	0x30	//Source of interrupts
#define	DATA_FORMAT	0x31	//Data format control
#define DATAX0		0x32	//X-Axis Data 0
#define DATAX1		0x33	//X-Axis Data 1
#define DATAY0		0x34	//Y-Axis Data 0
#define DATAY1		0x35	//Y-Axis Data 1
#define DATAZ0		0x36	//Z-Axis Data 0
#define DATAZ1		0x37	//Z-Axis Data 1
#define	FIFO_CTL	0x38	//FIFO control
#define	FIFO_STATUS	0x39	//FIFO status

int main (void){
int data=0;
int data1=0;
int data2=0;
int read=0;
int reads=0;
//*PINSEL1 &=~(3<<0);
*FIO0DIR |=(1<<16);
*FIO0SET |=(1<<16);
*FIO0CLR |=(1<<16);
*PINSEL0 |= (3<<30); //CONFIGURO P0.15 COMO SCK
*PINMODE0 |=(3<<30);//PULL DOWN RES
*PINMODE1 |=(255<<0); //PULL DOWN RES
//*PINSEL1 |= (3<<0); // ssel  CONFIGURO P0.16,P0.17,P0.18 , SSEL, MISO, MOSI
*PINSEL1 |=(3<<2);//MISO
*PINSEL1 |=(3<<4);//MOSI
*ISER0 |= (1<<13);   // ENABLE INTERRUPCIONES POR SPI
*PCLKSEL0 |= (3<<16);//SETEO EL PERIPHERAL CLOCK PARA SPI CCLCK/8

*S0SPCCR = 8; //peripheral clk /8
*S0SPCR |=(1<<2);//xBIT PER TRANSFER.
*S0SPCR&=~(15<<8);//16bit per trans
*FIO0SET |=(1<<16);
*S0SPCR |= (1<<4); //CLK POLARITY ACTIVE LOW  1
*S0SPCR |=(1<<5); //MASTER MODE SELECTED
*S0SPCR |=(1<<3);//cpha 1
*S0SPCR &=~(1<<6);


//**INICIALIZACION**//
*FIO0SET|=(1<<16);
*S0SPDR = 0x00;
while((*S0SPSR&(1<<7)==0));
			data=*S0SPDR;
			*FIO0SET|=(1<<16);
//**ENVIO**//
*FIO0CLR |=(1<<16);

*S0SPDR = 0x2C;
while((*S0SPSR&(1<<7)==0));
		data1=*S0SPDR;

*FIO0SET|=(1<<16);
*S0SPDR = 0x00;


//all  de nuevo

//**INICIALIZACION**//

*FIO0SET|=(1<<16);
*S0SPDR = 0x00;
while((*S0SPSR&(1<<7)==0));
			data=*S0SPDR;
			*FIO0SET|=(1<<16);
//**ENVIO**//
*FIO0CLR |=(1<<16);
*S0SPDR = 0x00;
while((*S0SPSR&(1<<7)==0));
		data2=*S0SPDR;
*FIO0SET|=(1<<16);
*S0SPDR = 0x00;



//all de nuevo

//**INICIALIZACION**//
*FIO0SET|=(1<<16);
*S0SPDR = 0x00;
while((*S0SPSR&(1<<7)==0));
			data=*S0SPDR;
			*FIO0SET|=(1<<16);

//**ENVIO**//
*FIO0CLR |=(1<<16);
*S0SPDR = 0x30;
while((*S0SPSR&(1<<7)==0));
		data=*S0SPDR;

*FIO0SET|=(1<<16);

*S0SPDR = 0x00;

 return 0;


}
