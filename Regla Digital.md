El trabajo Final consiste en desarrollar una Regla digital que proporciona una una distacia definida por el largo de la regla 
y el nivel de esta dado por  el acelerometro ADLX 345.
Los datos obtenidos por el ADLX 345 seran enviados por Bluetooth a una PC en la cual se procesaran para graficar el angulo 
de inclinacion Vs "tiempo real".
El angulo y la medcion (en grados y centimetros) seran mostrados por medio de displays en la regla, y los modos NIVEL Y MEDICION 
podran ser seleccionados a traves de pulsadores.


Para obtener la medicion  se disponen dos pulsadores ubicados en los extremos de la regla que al ser pulsados tomaran el largo 
de la regla. Cada vez que se pulsen esta distancia se sumara y se mostrara en los displays. Para borrar e iniciar una nueva medicion 
se debe precionar nuevamente el boton selector de modo MEDICION. 

Para el modo nivel se selecciona con el pulsador  "modo NIVEL" y los datos del ADLX seran tomados y enviados a la PC cada 5ms a traves
de un Handler de Timer.