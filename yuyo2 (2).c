#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#define AddrFIO0DIR 0x2009C000
#define AddrFIO0SET 0x2009C018
#define AddrFIO0CLR 0x2009C01C

unsigned int volatile *const FIO0DIR =(unsigned int *)AddrFIO0DIR;
unsigned int volatile *const FIO0SET =(unsigned int *)AddrFIO0SET;
unsigned int volatile *const FIO0CLR =(unsigned int*)AddrFIO0CLR;

#define AddrPCONP    0x400FC0C4
#define AddrPCLKSEL0 0x400FC1A8
#define AddrPINSEL0  0x4002C000
#define AddrPINSEL1  0x4002C004
#define AddrPINMODE  0x4002C040
#define AddrISER0	 0xE000E100


unsigned int volatile *const PCONP = (unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL0 = (unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PINSEL0 = (unsigned int*)AddrPINSEL0;
unsigned int volatile *const PINSEL1 = (unsigned int*)AddrPINSEL1;
unsigned int volatile *const PINMODE = (unsigned int*)AddrPINMODE;
unsigned int volatile *const ISER0 = (unsigned int*)AddrISER0;

//REGISTROS DE SPI

#define AddrS0SPCR  0x40020000
#define AddrS0SPSR  0x40020004
#define AddrS0SPDR  0x40020008
#define AddrS0SPCCR 0x4002000C
#define AddrS0SPINT 0x4002001C

unsigned int volatile *const S0SPCR =(unsigned int*)AddrS0SPCR;
unsigned int volatile *const S0SPSR =(unsigned int*)AddrS0SPSR;
unsigned int volatile *const S0SPDR =(unsigned int*)AddrS0SPDR;
unsigned int volatile *const S0SPCCR =(unsigned int*)AddrS0SPCCR;
unsigned int volatile *const S0SPINT =(unsigned int*)AddrS0SPINT;

//ADXL345 Register Addresses
#define	DEVID		0x00	//Device ID Register
#define THRESH_TAP	0x1D	//Tap Threshold
#define	OFSX		0x1E	//X-axis offset
#define	OFSY		0x1F	//Y-axis offset
#define	OFSZ		0x20	//Z-axis offset
#define	DURATION	0x21	//Tap Duration
#define	LATENT		0x22	//Tap latency
#define	WINDOW		0x23	//Tap window
#define	THRESH_ACT	0x24	//Activity Threshold
#define	THRESH_INACT	0x25	//Inactivity Threshold
#define	TIME_INACT	0x26	//Inactivity Time
#define	ACT_INACT_CTL	0x27	//Axis enable control for activity and inactivity detection
#define	THRESH_FF	0x28	//free-fall threshold
#define	TIME_FF		0x29	//Free-Fall Time
#define	TAP_AXES	0x2A	//Axis control for tap/double tap
#define ACT_TAP_STATUS	0x2B	//Source of tap/double tap
#define	BW_RATE		0x2C	//Data rate and power mode control
#define POWER_CTL	0x2D	//Power Control Register
#define	INT_ENABLE	0x2E	//Interrupt Enable Control
#define	INT_MAP		0x2F	//Interrupt Mapping Control
#define	INT_SOURCE	0x30	//Source of interrupts
#define	DATA_FORMAT	0x31	//Data format control
#define DATAX0		0x32	//X-Axis Data 0
#define DATAX1		0x33	//X-Axis Data 1
#define DATAY0		0x34	//Y-Axis Data 0
#define DATAY1		0x35	//Y-Axis Data 1
#define DATAZ0		0x36	//Z-Axis Data 0
#define DATAZ1		0x37	//Z-Axis Data 1
#define	FIFO_CTL	0x38	//FIFO control
#define	FIFO_STATUS	0x39	//FIFO status

//uint16_t bitread  = 0x00; // Bit 7 = 0 para leer
 //uint16_t bitwrite = 0x8000; // Bit 7 = 1 para escribir

int data=0;
int data1=0;
int data2=0;
int read=0;
int reads=0;
float res = 4.0/1024.0;
int main (void){



//********************************************CONFIGURACION SPI***************************************************************************
//*PINSEL1 &=~(3<<0);
*FIO0DIR |=(1<<16);
*FIO0SET |=(1<<16);
*FIO0CLR |=(1<<16);
*PINSEL0 |= (3<<30); //CONFIGURO P0.15 COMO SCK

//*PINSEL1 |= (3<<0); // ssel  CONFIGURO P0.16,P0.17,P0.18 , SSEL, MISO, MOSI
*PINSEL1 |=(3<<2);//MISO
*PINSEL1 |=(3<<4);//MOSI
*ISER0 |= (1<<13);   // ENABLE INTERRUPCIONES POR SPI
*PCLKSEL0 |= (3<<16);//SETEO EL PERIPHERAL CLOCK PARA SPI CCLCK/8

*S0SPCCR = 8; //peripheral clk /8
*S0SPCR |=(1<<2);//8BIT PER TRANSFER.
*S0SPCR &=~(15<<8);
*FIO0SET |=(1<<16);
*S0SPCR |= (1<<4); //CLK POLARITY ACTIVE LOW  1
*S0SPCR |=(1<<5); //MASTER MODE SELECTED
*S0SPCR |=(1<<3);//cpha 1


//****************************************************************************************************************************************

	writeADLX(DATA_FORMAT, 0x01);
	writeADLX(INT_ENABLE, 0x00);
	writeADLX(THRESH_INACT, 0x0F);
	writeADLX(TIME_INACT,0);
	writeADLX(ACT_INACT_CTL, 0x07);
	writeADLX(INT_ENABLE, 0x08);
	writeADLX(POWER_CTL, 0x08);

	while(1){
		Aceleraciones();
		if (*S0SPSR&(1<<6)==1){
			reads = *S0SPSR;
			reads =*S0SPSR;
			reads =*S0SPSR;
		}
	}

}

int SPI_transfer(int data){    //funcion de transferencia de datos

		*S0SPDR = data;
		while ((*S0SPSR&(1<<7)==0));
		data =  *S0SPDR;
	return data;
}

int readADLX(int regAdd){   //funcion para leer datos
	int read=0;
	*FIO0CLR |=(1<<16);
	uint16_t RDb = (((2<<14) | ((regAdd)&0x3F)<<8));
	SPI_transfer(RDb);
	read = *S0SPDR;
	*FIO0SET |=(1<<16);
	return read;
}

void writeADLX(int regAdd,int valor){   //funcion para escribir datos
	*FIO0CLR |=(1<<16);
	uint16_t WRb=0;
	WRb = ((regAdd)&0x3F)<<8 | (valor &0xFF);
	SPI_transfer(WRb);
	//SPI_transfer(valor);
	*FIO0SET |=(1<<16);
	retardo(10);
}

void Aceleraciones(void){
	//int yuyo = readADLX(DATAX0);
	int x0 = readADLX(DATAX0);
	int x1 = readADLX(DATAX1);
	int y0 = readADLX(DATAY0);
	int y1 = readADLX(DATAY1);
	int z0 = readADLX(DATAZ0);
	int z1 = readADLX(DATAZ1);
	uint16_t X1 = ((y0 & 7) << 8) | (x1 & 0xFF);
	uint16_t Y1 = ((z0 & 7)<<8) | (y1 & 0xFF);
	uint16_t Z1 = ((x0 & 7) << 8) | (z1 & 0xFF);

	float X = X1*res;
	float Y = Y1*res;
	float Z = Z1*res;

}


int retardo(unsigned int time){
	unsigned int i;
    for (i=0;i<time;i++);
    return 0;
}
