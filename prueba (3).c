#define AddrSCS 0x400FC1A0
#define AddrCCLKCFG 0x400FC104
#define AddrPCLKSEL0 0x400FC1A8
#define AddrPCLKSEL1 0x400FC1AC
#define AddrCLKSRCSEL 0x400FC10C
#define AddrPLL0CFG 0X400FC084
#define AddrPLL0FEED 0X400FC08C
#define AddrPLL0CON 0X400FC080
#define AddrPLL0STAT 0X400FC088

#define AddrPCONP 0X400FC0C4
#define AddrPINSEL0 0X4002C000
#define AddrU2LCR 0X4009800C
#define AddrU2DLL 0X40098000
#define AddrU2DLM 0X40098004
#define AddrU2IER 0X40098004
#define AddrISER0 0XE000E100
#define AddrU2THR 0X40098000
#define AddrU2LSR 0X40098014
#define AddrU2RBR 0X40098000

unsigned int volatile*const SCS =(unsigned int*)AddrSCS;//The SCS register contains several control/status bits related to the main oscillator
unsigned int volatile*const CCLKCFG =(unsigned int*)AddrCCLKCFG;
unsigned int volatile*const PCLKSEL0 =(unsigned int*)AddrPCLKSEL0;
unsigned int volatile*const PCLKSEL1 =(unsigned int*)AddrPCLKSEL1;
unsigned int volatile*const CLKSRCSEL =(unsigned int*)AddrCLKSRCSEL;
unsigned int volatile*const PLL0CFG =(unsigned int*)AddrPLL0CFG;
unsigned int volatile*const PLL0FEED =(unsigned int*)AddrPLL0FEED;
unsigned int volatile*const PLL0CON =(unsigned int*)AddrPLL0CON;
unsigned int volatile*const PLL0STAT =(unsigned int*)AddrPLL0STAT;

unsigned int volatile*const PCONP =(unsigned int*)AddrPCONP;
unsigned int volatile*const PINSEL0 =(unsigned int*)AddrPINSEL0;
unsigned int volatile*const U2LCR =(unsigned int*)AddrU2LCR;
unsigned int volatile*const U2DLL =(unsigned int*)AddrU2DLL;
unsigned int volatile*const U2DLM =(unsigned int*)AddrU2DLM;
unsigned int volatile*const U2IER =(unsigned int*)AddrU2IER;
unsigned int volatile*const ISER0 =(unsigned int*)AddrISER0;
unsigned int volatile*const U2THR =(unsigned int*)AddrU2THR;
unsigned int volatile*const U2LSR =(unsigned int*)AddrU2LSR;
unsigned int volatile*const U2RBR =(unsigned int*)AddrU2RBR;

#define AddrT0EMR 0x4000403C
#define AddrT0MRC 0x40004014
#define AddrT0MR0 0x40004018
#define AddrT0TCR 0x40004004
#define AddrT0IR 0x40004000
#define AddrT1MR0 0x40008018
#define AddrT1TCR 0x40008004
#define AddrT1IR 0x40008000
#define AddrT1MCR 0x40008014

unsigned int volatile *const T0EMR = (unsigned int*) AddrT0EMR;
unsigned int volatile *const T0MCR = (unsigned int*) AddrT0MRC;
unsigned int volatile *const T0MR0 = (unsigned int*) AddrT0MR0;
unsigned int volatile *const T0TCR = (unsigned int*) AddrT0TCR;
unsigned int volatile *const T0IR = (unsigned int*) AddrT0IR;
unsigned int volatile *const T1MR0 = (unsigned int*) AddrT1MR0;
unsigned int volatile *const T1TCR = (unsigned int*) AddrT1TCR;
unsigned int volatile *const T1IR = (unsigned int*) AddrT1IR;
unsigned int volatile *const T1MCR = (unsigned int*)AddrT1MCR;

#define AddrFIO0DIR 0x2009C000
#define AddrFIO0SET 0x2009C018
#define AddrFIO0CLR 0x2009C01C

#define AddrFIO2DIR 0x2009C040
#define AddrFIO2SET 0x2009C058
#define AddrFIO2CLR 0x2009C05C
#define AddrPINSEL3 0x4002C00C
#define AddrPINSEL4  0x4002C010
unsigned int volatile *const FIO2DIR = (unsigned int*)AddrFIO2DIR;
unsigned int volatile *const FIO2SET = (unsigned int*)AddrFIO2SET;
unsigned int volatile *const FIO2CLR = (unsigned int*)AddrFIO2CLR;
unsigned int volatile *const PINSEL3 = (unsigned int*)AddrPINSEL3;
unsigned int volatile *const FIO0DIR =(unsigned int *)AddrFIO0DIR;
unsigned int volatile *const FIO0SET =(unsigned int *)AddrFIO0SET;
unsigned int volatile *const FIO0CLR =(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const PINSEL4 = (unsigned int*)AddrPINSEL4;
#define AddrEXTINT   0x400FC140
#define AddrEXTMODE  0X400FC148
#define AddrEXTPOLAR 0x400FC14C
#define AddrIO0IntEnF 0x40028094
#define AddrIO0IntClr 0X4002808C
#define AddrIO0IntStatF 0x40028088

unsigned int volatile *const EXTINT =(unsigned int *)AddrEXTINT;
unsigned int volatile *const EXTPOLAR =(unsigned int*)AddrEXTPOLAR;
unsigned int volatile *const EXTMODE =( unsigned int*)AddrEXTMODE;
unsigned int volatile *const IO0IntEnF =(unsigned int*)AddrIO0IntEnF;
unsigned int volatile *const IO0IntClr =(unsigned int*)AddrIO0IntClr;
unsigned int volatile *const IO0IntStatF = (unsigned int*)AddrIO0IntStatF;



#define AddrAD0CR    0x40034000//REGISTRO DE CONTROL DEL A/D
#define AddrAD0INTEN 0x4003400C//REGISTRO DE INTERRUPCIONES DEL A/D
#define AddrPINMODE1 0X4002C044//para nither pull up/down
#define AddrPINSEL1  0x4002C004//para establecer el pin 23 como ad0
#define AddrAD0DR0   0x40034010 //CHANNEL 0 DATA REGISTER


#define AddrICER0	 0xE000E180



unsigned int volatile *const AD0CR =(unsigned int*)AddrAD0CR;
unsigned int volatile *const AD0INTEN =(unsigned int*)AddrAD0INTEN;
unsigned int volatile *const PINMODE1 =(unsigned int*)AddrPINMODE1;
unsigned int volatile *const PINSEL1 =(unsigned int*)AddrPINSEL1;
unsigned int volatile *const AD0DR0 =(unsigned int*)AddrAD0DR0;


unsigned int volatile *const ICER0 =(unsigned int*)AddrICER0;
//VARIABLES//
int REGflag=0;
int ADCflag=0;
int dato=0;
int i=0;
int decenas=0;
int centenas=0;
int unidades=0;
//*************************
void EINTConfig(void);
void ADCConfig(void);
void TIMER0Config(void);
void TIMER1Config(void);

int main(void) {
	*FIO2DIR |=(1<<7); //D1
	*FIO2DIR |=(1<<8);  //D2
	*FIO2DIR |=(1<<13); //D3
	*FIO2DIR |=(127);   //PINES SEGMENTOS
	*FIO2SET |=127;    //APAGO TODOS LOS SEGMENTOS

	EINTConfig();
	ADCConfig();
	TIMER0Config();
	TIMER1Config();

	while (1){

		         centenas=dato/100;
				 decenas=(dato-( centenas*100))/10;
				 unidades=dato-( centenas*100 + decenas*10 );

		if (ADCflag==1){
			*ISER0 |=(1<<22);  //start now

		}


	}


	return 0;
}
void EINT0_IRQHandler(void){ //REG P2.10
	dato=0;
	REGflag = 1;
	ADCflag = 0;
	*ICER0 =(1<<22);				//BAJO INTR ADC
	//*ICER0 =(1<<2);						//BAJO INTR TIMER 1
    *EXTINT |=(1<<0);
}

void EINT1_IRQHandler(void){ //NIV P2.11
	dato=0;
	REGflag =0;
	ADCflag=1;                       //INTR DE ADC
	//INTR DE TIMER 1
	*EXTINT |=(1<<1);
}

void EINT2_IRQHandler(void){ //MED P2.12
	if (REGflag ==1){
		dato +=16;
	}
	*EXTINT |=(1<<2);
}

void ADC_IRQHandler(void){
	dato = (*AD0DR0>>4)&0xFFF;
	dato=dato/22.75;
	*ICER0 =(1<<22);
	//*AD0CR |=(1<<24);

}
void TIMER0_IRQHandler(void){

	if (i == 0){
			*FIO2SET|= 127;
		    *FIO2SET |=(1<<7);//D1 ON
			*FIO2CLR |=(1<<8);//D2 OFF
			*FIO2CLR |=(1<<13);//D3 OFF
            Display(unidades);
			*T0IR |=(1<<0);
			i=1;
}
			else if (i == 1){//UNIDADES
				*FIO2SET|= 127;
				*FIO2CLR |=(1<<7);//D1 OFF
				*FIO2SET |=(1<<8);//D2 ON
				*FIO2CLR |=(1<<13);//D3 OFF
                 Display(decenas);
                *T0IR |=(1<<0);
				i=2;
			}
			else if (i==2){
				*FIO2SET|= 127;
				*FIO2CLR |=(1<<7);//D1 OFF
				*FIO2CLR |=(1<<8);//D2 OFF
				*FIO2SET |=(1<<13);//D3 ON
                Display (centenas);
                *T0IR |= (1<<0);
                i=0;
	}



}
void TIMER1_IRQHandler(void){

	*T1IR |= 1;

}
//***************************** CONFIGURACIONES*********************************************************************************************//
void EINTConfig (){

	    //EINT0 P2.10    BOTON A REG
		*PINSEL4 |=(1<<20);
		*FIO2DIR &=~(1<<10);
		*EXTMODE |=(1<<0);
		*EXTPOLAR &=~(1<<0);
		*ISER0 |=(1<<18);
		*EXTINT |=(1<<0);
		//EINT1 P2.11    BOTON B NIV
		*PINSEL4 |=(1<<22);
		*FIO2DIR &=~(1<<11);
		*EXTMODE |=(1<<1);
		*EXTPOLAR &=~(1<<1);
		*ISER0 |=(1<<19);
		*EXTINT |=(1<<1);
		//EINT2 P2.12    BOTON C MED
		*PINSEL4 |=(1<<24);
	    *FIO2DIR &=~(1<<12);
	    *EXTMODE |=(1<<2);
	    *EXTPOLAR &=~(1<<2);
	    *ISER0 |=(1<<20);
		*EXTINT |=(1<<2);
}

void ADCConfig(){
	    *PCONP|= (1<<12);   			//Prender ADC
		*PCLKSEL0|= (0<<24); 			//selecciona pclock_adc/1
		*PINSEL1|= (1<<14); 			// P0.23 = 01  selecciona AD0.0
		*AD0CR|= (1<<0);    			//selecciona el canal AD0.0
		*AD0CR|= (1<<21);				//ADC operacional
		*AD0CR|=(1<<16);				//deshabilita el  modo burst
		//*ISER0 |=(1<<22);
		*AD0INTEN &=~(1<<8);
		*AD0INTEN |=1;              //interrupcion de fin de conversion
}
void TIMER0Config (){ //1ms

	*PCLKSEL0 |=(1<<2);											//PERIPHERAL CCLK/1
	*T0MCR = 3;
	*T0MR0 = 100000;
	*ISER0 |=(1<<1);
	*T0TCR |= 0;
	*T0TCR |=1;
	*T0IR = 1;
}
void TIMER1Config(){
	*PCLKSEL0 |=(1<<4);
	*T1MCR = 3 ;
	*T1MR0 = 500000;
	*T1TCR |=0;
	*T1TCR |= 1;
	*T1IR = 1;
}

//********************************************************************************************************************************************//

void Display (int n){
		switch(n){
		case 0:
			*FIO2CLR |= (1<<0); // C
			*FIO2CLR |= (1<<1);  // D
			*FIO2CLR |= (1<<2);     //E
			*FIO2CLR |= (1<<3); //B
			*FIO2CLR |= (1<<4);   //A
			*FIO2CLR |= (1<<5);  //F
			*FIO2SET |= (1<<6); //G

			break;
		case 1:
			*FIO2CLR |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2SET |= (1<<6);

			break;
		case 2:
			*FIO2SET |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2CLR |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2CLR |= (1<<6);

			break;
		case 3:
			*FIO2CLR |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2CLR |= (1<<6);

			break;
		case 4:
			*FIO2CLR |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2CLR |= (1<<6);

			break;
		case 5:
			*FIO2CLR |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);//B
			*FIO2CLR |= (1<<4);//A
			*FIO2CLR |= (1<<5);//F
			*FIO2CLR |= (1<<6);//G

			break;
		case 6:
			*FIO2CLR |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2CLR |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2CLR |= (1<<6);

			break;
		case 7:
			*FIO2CLR |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2SET |= (1<<6);

			break;
		case 8:
			*FIO2CLR |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2CLR |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2CLR |= (1<<6);

			break;
		case 9:
			*FIO2CLR |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2CLR |= (1<<6);

		}
	}

