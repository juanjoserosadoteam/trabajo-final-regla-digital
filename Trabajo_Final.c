#define AddrSCS 0x400FC1A0
#define AddrCCLKCFG 0x400FC104
#define AddrPCLKSEL0 0x400FC1A8
#define AddrPCLKSEL1 0x400FC1AC
#define AddrCLKSRCSEL 0x400FC10C
#define AddrPLL0CFG 0X400FC084
#define AddrPLL0FEED 0X400FC08C
#define AddrPLL0CON 0X400FC080
#define AddrPLL0STAT 0X400FC088

#define AddrPCONP 0X400FC0C4
#define AddrPINSEL0 0X4002C000
#define AddrU2LCR 0X4009800C
#define AddrU2DLL 0X40098000
#define AddrU2DLM 0X40098004
#define AddrU2IER 0X40098004
#define AddrISER0 0XE000E100
#define AddrU2THR 0X40098000
#define AddrU2LSR 0X40098014
#define AddrU2RBR 0X40098000

unsigned int volatile*const SCS =(unsigned int*)AddrSCS;//The SCS register contains several control/status bits related to the main oscillator
unsigned int volatile*const CCLKCFG =(unsigned int*)AddrCCLKCFG;
unsigned int volatile*const PCLKSEL0 =(unsigned int*)AddrPCLKSEL0;
unsigned int volatile*const PCLKSEL1 =(unsigned int*)AddrPCLKSEL1;
unsigned int volatile*const CLKSRCSEL =(unsigned int*)AddrCLKSRCSEL;
unsigned int volatile*const PLL0CFG =(unsigned int*)AddrPLL0CFG;
unsigned int volatile*const PLL0FEED =(unsigned int*)AddrPLL0FEED;
unsigned int volatile*const PLL0CON =(unsigned int*)AddrPLL0CON;
unsigned int volatile*const PLL0STAT =(unsigned int*)AddrPLL0STAT;

unsigned int volatile*const PCONP =(unsigned int*)AddrPCONP;
unsigned int volatile*const PINSEL0 =(unsigned int*)AddrPINSEL0;
unsigned int volatile*const U2LCR =(unsigned int*)AddrU2LCR;
unsigned int volatile*const U2DLL =(unsigned int*)AddrU2DLL;
unsigned int volatile*const U2DLM =(unsigned int*)AddrU2DLM;
unsigned int volatile*const U2IER =(unsigned int*)AddrU2IER;
unsigned int volatile*const ISER0 =(unsigned int*)AddrISER0;
unsigned int volatile*const U2THR =(unsigned int*)AddrU2THR;
unsigned int volatile*const U2LSR =(unsigned int*)AddrU2LSR;
unsigned int volatile*const U2RBR =(unsigned int*)AddrU2RBR;

#define AddrT0EMR 0x4000403C
#define AddrT0MRC 0x40004014
#define AddrT0MR0 0x40004018
#define AddrT0TCR 0x40004004
#define AddrT0IR 0x40004000
#define AddrT1MR0 0x40008018
#define AddrT1TCR 0x40008004
#define AddrT1IR 0x40008000
#define AddrT1MCR 0x40008014

unsigned int volatile *const T0EMR = (unsigned int*) AddrT0EMR;
unsigned int volatile *const T0MCR = (unsigned int*) AddrT0MRC;
unsigned int volatile *const T0MR0 = (unsigned int*) AddrT0MR0;
unsigned int volatile *const T0TCR = (unsigned int*) AddrT0TCR;
unsigned int volatile *const T0IR = (unsigned int*) AddrT0IR;
unsigned int volatile *const T1MR0 = (unsigned int*) AddrT1MR0;
unsigned int volatile *const T1TCR = (unsigned int*) AddrT1TCR;
unsigned int volatile *const T1IR = (unsigned int*) AddrT1IR;
unsigned int volatile *const T1MCR = (unsigned int*)AddrT1MCR;

#define AddrFIO0DIR 0x2009C000
#define AddrFIO0SET 0x2009C018
#define AddrFIO0CLR 0x2009C01C

#define AddrFIO2DIR 0x2009C040
#define AddrFIO2SET 0x2009C058
#define AddrFIO2CLR 0x2009C05C
#define AddrPINSEL3 0x4002C00C

unsigned int volatile *const FIO2DIR = (unsigned int*)AddrFIO2DIR;
unsigned int volatile *const FIO2SET = (unsigned int*)AddrFIO2SET;
unsigned int volatile *const FIO2CLR = (unsigned int*)AddrFIO2CLR;
unsigned int volatile *const PINSEL3 = (unsigned int*)AddrPINSEL3;
unsigned int volatile *const FIO0DIR =(unsigned int *)AddrFIO0DIR;
unsigned int volatile *const FIO0SET =(unsigned int *)AddrFIO0SET;
unsigned int volatile *const FIO0CLR =(unsigned int*)AddrFIO0CLR;

#define AddrEXTINT   0x400FC140
#define AddrEXTMODE  0X400FC148
#define AddrEXTPOLAR 0x400FC14C
#define AddrIO0IntEnF 0x40028094
#define AddrIO0IntClr 0X4002808C
#define AddrIO0IntStatF 0x40028088

unsigned int volatile *const EXTINT =(unsigned int *)AddrEXTINT;
unsigned int volatile *const EXTPOLAR =(unsigned int*)AddrEXTPOLAR;
unsigned int volatile *const EXTMODE =( unsigned int*)AddrEXTMODE;
unsigned int volatile *const IO0IntEnF =(unsigned int*)AddrIO0IntEnF;
unsigned int volatile *const IO0IntClr =(unsigned int*)AddrIO0IntClr;
unsigned int volatile *const IO0IntStatF = (unsigned int*)AddrIO0IntStatF;



void Timer0Config();
void Timer1Config();
void UARTConfig();
void GPIOConfig();
void ClockConfig();
void PinConfig();
void Multiplex();
void Enviar(int);

int short j;
int short i;
int volatile  Dato=0;
int unidades=0;
int decenas=0;
int centenas=0;

int main(void){



	while(1){


		int centenas=Dato/100;
		int decenas=(Dato-( centenas*100))/10;
		int unidades=Dato-( centenas*100 + decenas*10 );




		}

	}


void Timer1Config(void){//5ms
	*ISER0 |= (1<<2);
	*PCLKSEL0 |=(2<<4); //CCLK/2
	*T1TCR |=(1<<0); //timer counter ready for counting
	//*T1MCR |= 3;    //interrupcion y reseteo cuando el TC matches MR0
	*T1MR0  |= (250000);
}
void Timer0Config (void){//configuracion para el multiplexado de los displays
	*ISER0 |= (1<<1);// Interrupciones por timer0 habilitadas
	*PCLKSEL0 |=(1<<2); //PCLK_peripheral = CCLK
	*T0TCR |=(1<<0); //timer counter ready for counting
	*T0MCR |= 3;    //interrupcion y reseteo cuando el TC matches MR0
	*T0MR0  |= (13000);
}

void PinConfig(void){

	*FIO0DIR &=~(1<<23);//Boton A modo med       |
	*FIO0DIR &=~(1<<24);//Boton B modo Nivel     |ENTRADAS
	*FIO0DIR &=~(1<<25);//Boton 1 regla			 |
	*FIO0DIR &=~(1<<26);//Boton 2 regla          |

	*EXTMODE |= (1<<3); //EDGE SENSITIVE
	*EXTPOLAR &=~(1<<3);//FALL

}
void GPIOConfig(void){
	*IO0IntEnF |=(3<<23);//interrupciones para botones A y B

}
void EXINT3_IRQHandler(){

	if ((*IO0IntStatF>>23) & 1){ //interrupcion por Boton A (P0.23) MODO REGLA
		*IO0IntEnF |=(3<<25);//enable interrupt for Boton 1 y 2
		//deshabilitar el envio de Datos
		*T1MCR &=~(1<<3); //deshabilito las interrupciones para el match de timer1
		//j=0;
		Dato=0;
		*IO0IntClr |=(1<<23); //bajo bandera de interrupcion Boton A
	}

	if ((*IO0IntStatF>>24) & 1){ //interrupcion por Boton B (P0.24) MODO NIVEL
		*IO0IntEnF &=~(3<<25); //disable interrupt for Boton 1 y 2
		Dato=0;
		//j=1;
		*T1MCR |=(1<<3);//habilito las interr por match1 para enviar Dato a PC cada 5ms
		*IO0IntClr |=(1<<24);//bajo bandera
	}
	if (((*IO0IntStatF>>25)||(*IO0IntStatF>>26)) & 1){//interrupcion por botones de regla (P0.25 y P0.26)
		Dato =+ 20;  //sumo el largo de la regla
		*IO0IntClr |=(1<<25);
		*IO0IntClr |=(1<<26); //bajo banderas de interr

	}
}
void TIMER1_IRQHandler(){//enviar Dato cada 5ms
	enviar(Dato);
	*T1IR |=(1<<1);

}
void TIMER0_IRQHandler(){

    Multiplex();
        }

void enviar (int Dato){

	while ((*U2LSR&(1<<5))==0);//check if UnTHR contains valid data or is empty
	*U2THR=Dato; //EL PROXIMO Dato A SER TRANSMITIDO SE ESCRIBE ACA
}


void ClockConfig (void)//100Mhz
{
	*SCS=32;//main osc is enable
	while ((*SCS &(1<<6))==0);//chequeo si el bit de OSCSTAT esta en 1, si lo esta el main osc esta listo para usar como fuente de clock

	*CCLKCFG= 0x3;//pllclk is divided by 4 to produce the CPU clock.

	*PCLKSEL0= 0x0;
	*PCLKSEL1= 0x0;

	*CLKSRCSEL= 0x1;// selecciono el main osc como el clk para el pll0

	*PLL0CFG= 0x50063;//99,5(100,6)
	*PLL0FEED= 0xAA;//secuencias para el correcto funcionamiento
	*PLL0FEED= 0x55;//****************************************

	*PLL0CON= 0x01;//PLL0 enable.When one, and after a valid PLL0 feed, this bit will activate
	                 //PLL0 and allow it to lock to the requested frequency
	*PLL0FEED= 0xAA;
	*PLL0FEED= 0x55;
	while(!(*PLL0STAT &(1<<26)));

	*PLL0CON = 0x03;//PLL0 Connect
	*PLL0FEED= 0xAA;
	*PLL0FEED = 0x55;
	while(!(*PLL0STAT &((1<<25)|(1<<24))));
}
void Multiplex(){


	if (i == 0){
			    *FIO2CLR |=(1<<8);
				*FIO2SET |=(1<<7);
				*FIO2SET |=(1<<9);
				*FIO2SET|= 127;

				Display(unidades);
				*T0IR |=(1<<0);
				i=1;


				}
				else if (i == 1){
					*FIO2CLR |=(1<<7);
					*FIO2SET |=(1<<8);
					*FIO2SET |=(1<<9);
					*FIO2SET|= 127;


					Display(decenas);

					*T0IR |=(1<<0);
					i=2;
				}
				else if (i == 2){
					*FIO2CLR |=(1<<9);
					*FIO2SET |=(1<<8);
					*FIO2SET |=(1<<7);
					*FIO2SET|= 127;


					Display(centenas);

					*T0IR |=(1<<0);
					i=0;
				}
}

void Display (int n){
		switch(n){
		case 0:
			*FIO2SET |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2CLR |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 1:
			*FIO2SET |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2CLR |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2CLR |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 2:
			*FIO2CLR |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 3:
			*FIO2SET |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 4:
			*FIO2SET |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2CLR |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 5:
			*FIO2SET |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 6:
			*FIO2SET |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2CLR |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 7:
			*FIO2SET |= (1<<0);
			*FIO2CLR |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2CLR |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 8:
			*FIO2SET |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2SET |= (1<<4);
			*FIO2SET |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
			break;
		case 9:
			*FIO2SET |= (1<<0);
			*FIO2SET |= (1<<1);
			*FIO2SET |= (1<<2);
			*FIO2SET |= (1<<3);
			*FIO2CLR |= (1<<4);
			*FIO2CLR |= (1<<5);
			*FIO2SET |= (1<<6);
			*FIO0SET |= (1<<11);
		}
	}

